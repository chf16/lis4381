<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Simple calculator.">
	<meta name="author" content="Conner Fitzpatrick">
	<link rel="icon" href="favicon.ico">

	<title>ReWrite File</title>
	<?php include_once("../css/include_css.php"); ?>

</head>
<body>
<?php include_once("../global/nav.php"); ?>

<div class="container">
	<div class="starter-template">
					
			<div class="page-header">
				<?php include_once("global/header.php"); ?>	
			</div>
    <?php
    $num1 = $_POST['num1'];
    $num2 = $_POST['num2'];
    $operator = $_POST["operator"];
    echo '<h2>'. "$operator". '</h2>';
    switch($operator)
    {
        case "addition":
        echo "$num1 + $num2 = ";
        echo $num1 + $num2;
        break;

        case "subtraction":
        echo "$num1 - $num2 = ";
        echo $num1 - $num2;
        break;

        case "multiplication":
        echo "$num1 * $num2 = ";
        echo $num1 * $num2;
        break;

        case "division":
        if ($num2 != 0) {
            echo "$num1 / $num2 = ";
            echo $num1 / $num2;            
        }
        else {
            echo "Cannot divide by zero!";
       }
        break;

        case "exponentiation":
        echo "$num1 raised to the power of $num2 = ";
        echo pow($num1, $num2);
        break;

        default:
        echo "Must select an operation!";
    }
    ?>
    <p>&nbsp;</p>
    <?php include_once "global/footer.php"; ?>
			
	</div> <!-- end starter-template -->
 </div> <!-- end container -->
