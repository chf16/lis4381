<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Simple calculator.">
	<meta name="author" content="Conner Fitzpatrick">
	<link rel="icon" href="favicon.ico">

	<title>ReWrite File</title>
	<?php include_once("../css/include_css.php"); ?>

</head>
<body>
<?php include_once("../global/nav.php"); ?>
<p class="text-justify">
<div class="container">
<div class="starter-template">
<div class="page-header">
<?php include_once("global/header.php"); ?>
</div>


<p class="text-justify">
<?php

$myfile = fopen("file.txt", "w+") or exit("Unable to open file!");
$txt = $_POST['comment'];
fwrite($myfile, $txt);
fclose($myfile);

$myfile = fopen("file.txt", "r+") or exit("Unable to open file!");
while(!feof($myfile)) {
    echo fgets($myfile) . "<br />";
}

fclose($myfile);
?>

</p>

<p>&nbsp;</p>
    <?php include_once "global/footer.php"; ?>
			
	</div> <!-- end starter-template -->
 </div> <!-- end container -->