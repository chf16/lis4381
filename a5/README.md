# LIS4381 - Mobile Web Application Development

## Conner Fitzpatrick

### Assignment 5 Requirements:

*Four Parts:*

1. Requires A4 cloned files.
2. Review subdirectories and files
3. Open index.php and review code:
4. Answer chapter questions


#### README.md file should include the following items:

* Course title, your name, assignment requirements;
* Screenshot as per examples;
* Link to local lis4381 web app: http://localhost/repos/lis4381/;

#### Assignment Screenshots:

*Screenshot of Assignment 5 (index.php)*:

![Index Page](img/index.png) 

*Screenshot of add_petstore.php*:

![Add Petstore](img/add.png)

*Screenshot of add_petstore_process.php*:

![Error Page](img/error.png)

#### Links:

*a) Assignment:*
[A5 Link](https://bitbucket.org/chf16/lis4381/src/master/a5/)

*b) Local LIS4381 Web App:*
[Localhost Link](http://localhost/repos/lis4381/)



